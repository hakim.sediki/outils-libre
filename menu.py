#coding:utf-8
from hashlib import sha256
from tkinter import *
from tkinter import filedialog
from PIL import ImageTk,Image
#from baseDeDonnee import *

root = Tk()
root.geometry("680x480")
root.title("Save File")

my_image = PhotoImage(file="p8.png")
my_image_label = Label(root, image=my_image).pack()

lb = Label(root, text = "Enter the data file : .txt")
lb.pack()

def openDataFile():
    file_enter = filedialog.askopenfilename(initialdir="/python/tkinetr/", title="select A File", filetypes=(("Txt files", "*.txt"),("All files ", "*.*"), ("Data Files", "*")))
    #print(root.filename)
    #root.title(root.filename)
    content = open(file_enter).read()
    T = Text(root, height = 15, width = 60)
    T.pack()
    T.insert(END, content)

button_1 = Button(root, text = "Read file Content", padx = 110, pady = 10, command = openDataFile)
button_1.pack()     #grid(row = 2, column = 1)
   

lb1 = Label(root, text = "Enter the string key : ")
lb1.pack()                 #grid(row = 0, column = 0)

key_enter = Entry(root, width = 35, borderwidth = 5)
key_enter.pack()                       #grid(row = 0, column = 1, columnspan = 3, padx = 10, pady = 10)

key = key_enter.get()

keys = sha256(key.encode("utf-8")).digest()

#keys = sha256(k.encode("utf-8")).digest() 


lb2 = Label(root, text = "Click to encrypt the enter file : ")
lb2.pack()  

def encryption():
    b = b""
    with open(filedialog.askopenfilename(initialdir="/python/tkinetr/", title="select A File", filetypes=(("txt files", "*.txt"),("All files ", "*.*"),("Data Files", "*"))), "rb") as file_enter: #rb:read binary
        with open(filedialog.asksaveasfilename(defaultextension = ".txt", filetypes = (("Text File", "*.txt"),("All Files", "*.*"))), "wb") as file_out:
            i = 0
            while file_enter.peek():
                c = ord(file_enter.read(1))
                j = i % len(keys)
                res = bytes([c^keys[j]])
                file_out.write(b)
                i += 1
                b = b + res
            T = Text(root, height = 15, width = 60)
            T.pack()
            T.insert(END,"Voila le résultat en binaire : \n\n\n")
            T.insert(END, b)
    return b

#resultFuntction = encryption()

button_3 = Button(root, text = "Encrypt", padx = 110, pady = 10, command = encryption)
button_3.pack()                               #grid(row = 1, column = 1) 

def show_about():
    about_window = Toplevel(root)
    about_window.title("À PROPOS")
    lb=Label(about_window, text="Welcome to the top level window")
    lb.pack()

#widget
mainmenu = Menu(root)

first_menu = Menu(mainmenu, tearoff=0)            #eviter la bar discontinue
second_menu = Menu(mainmenu, tearoff=0)

first_menu.add_command(label="Quitter", command=root.quit)
second_menu.add_command(label="option2", command=show_about())

mainmenu.add_cascade(label="premier", menu=first_menu)
mainmenu.add_cascade(label="seconde", menu=second_menu)

#boucle principale
root.config(menu=mainmenu)
root.mainloop()